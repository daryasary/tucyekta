# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2019-01-24 16:49
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('partners', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('reports', '0006_activity_duration_seconds'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserActivities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('partner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='partners.Partner')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='useractivities',
            unique_together=set([('user', 'partner')]),
        ),
    ]
