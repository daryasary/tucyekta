from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class ReportsConfig(AppConfig):
    name = 'reports'
    verbose_name = _("report")
    verbose_name_plural = _("reports")
