from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from reports.views import ActivitiesListView, delete_activity

urlpatterns = [
    url(r'^$', login_required(ActivitiesListView.as_view()), name='activities_list'),
    url(r'^(?P<aid>[0-9]+)/$', login_required(delete_activity), name='delete_activity'),
]
