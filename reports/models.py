from django.db import models
from django.db.models import Sum
from khayyam import JalaliDatetime

from partners.models import Partner

from django.utils.translation import ugettext_lazy as _

from partners.utils import prettify
from users.models import User


class Activity(models.Model):
    user = models.ForeignKey(
        User, related_name='activities', verbose_name=_("User")
    )
    partner = models.ForeignKey(
        Partner, related_name='activities', verbose_name=_("partner")
    )
    start_time = models.DateTimeField(verbose_name=_('start time'))
    end_time = models.DateTimeField(verbose_name=_('end time'))
    duration_seconds = models.IntegerField(
        verbose_name=_("duration"), help_text=_("Duration in seconds unit")
    )

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("activity")
        verbose_name_plural = _("activities")

    def __str__(self):
        return '{} ==> {}'.format(self.user, self.partner)

    def save(self, **kwargs):
        self.duration_seconds = (self.end_time - self.start_time).seconds
        UserActivities.objects.get_or_create(user=self.user, partner=self.partner)
        super().save(**kwargs)

    @property
    def date(self):
        return JalaliDatetime(self.start_time).today().strftime('%A %d %B %Y')

    @property
    def start(self):
        return JalaliDatetime(self.start_time).strftime('%A %d %B %Y %H:%M')

    @property
    def end(self):
        return JalaliDatetime(self.end_time).strftime('%A %d %B %Y %H:%M')

    @property
    def duration(self):
        return prettify(self.duration_seconds)


class UserActivities(models.Model):
    user = models.ForeignKey(User)
    partner = models.ForeignKey(Partner)

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("User Activities")
        verbose_name_plural = _("Users Activities")
        unique_together = (('user', 'partner'),)

    def total(self):
        activities_time_sum = self.partner.activities.filter(
            user=self.user
        ).aggregate(Sum('duration_seconds'))
        return prettify(int(activities_time_sum['duration_seconds__sum'] or 0))
