from django.contrib import admin

from reports.models import Activity
from django.utils.translation import ugettext_lazy as _


class ActivityAdmin(admin.ModelAdmin):
    list_display = [
        'user_identity', 'partner', 'get_start', 'get_end', 'pretty_duration'
    ]
    list_filter = ['partner', 'user']
    search_fields = ['user__username', 'partner__name']

    def user_identity(self, obj):
        return obj.user.get_full_name()
    user_identity.short_description = _("user")

    def pretty_duration(self, obj):
        return obj.duration
    pretty_duration.short_description = _("duration")

    def get_start(self, obj):
        return obj.start
    get_start.short_description = _("start")

    def get_end(self, obj):
        return obj.end
    get_end.short_description = _("end")


admin.site.register(Activity, ActivityAdmin)
