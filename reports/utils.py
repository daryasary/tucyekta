from django.utils.translation import ugettext_lazy as _


def day_msg(i):
    if i == 0:
        return _("Today")
    if i == 1:
        return _("Yesterday")
    return '%i days ago' % i
