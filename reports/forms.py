from datetime import timedelta

from django import forms
from django.conf import settings
from django.utils import timezone

from partners.models import Partner
from reports.models import Activity
from django.utils.translation import ugettext_lazy as _

from reports.utils import day_msg


class ActivityForm(forms.ModelForm):
    DAY_CHOICES = [(i, day_msg(i)) for i in range(settings.ALLOWED_DAY)]
    start_time = forms.TimeField(label=_("Start"), input_formats=['%H:%M', '%H'], required=True)
    end_time = forms.TimeField(label=_("end"), input_formats=['%H:%M',  '%H'], required=True)
    day = forms.ChoiceField(
        label=_("day"), choices=DAY_CHOICES, widget=forms.RadioSelect()
    )
    partner = forms.ModelChoiceField(
        queryset=Partner.objects.all(), widget=forms.HiddenInput()
    )

    class Meta:
        model = Activity
        fields = ['partner', 'day', 'start_time', 'end_time']

    def clean(self):
        self._validate_unique = True
        if ('start_time' not in self.cleaned_data.keys()) or ('end_time' not in self.cleaned_data.keys()):
            raise forms.ValidationError(_("Start and End time is not provided"))
        start = self.cleaned_data.pop('start_time')
        end = self.cleaned_data.pop('end_time')
        if start > end:
            raise forms.ValidationError(_("Start time cannot be after and end"))
        self.cleaned_data['start_time'] = timezone.now().replace(
            hour=start.hour, minute=start.minute, second=0
        ) - timedelta(days=int(self.cleaned_data['day']))
        self.cleaned_data['end_time'] = timezone.now().replace(
            hour=end.hour, minute=end.minute, second=0
        ) - timedelta(days=int(self.cleaned_data['day']))
        return self.cleaned_data
