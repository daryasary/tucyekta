from django.db.models import Sum
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic import ListView, DeleteView

from partners.utils import prettify
from reports.models import Activity


class ActivitiesListView(ListView):
    model = Activity
    ordering = '-id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        activities_times = Activity.objects.filter(
            user=self.request.user
        ).aggregate(Sum('duration_seconds'))
        context['aggregated_time'] = prettify(
            int(activities_times['duration_seconds__sum'] or 0)
        )
        return context


def delete_activity(request, aid):
    try:
        activity = Activity.objects.get(pk=aid)
    except Activity.DoesNotExist():
        return redirect(reverse_lazy('activities_list'))
    activity.delete()
    return redirect(reverse_lazy('activities_list'))
