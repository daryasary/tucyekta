def prettify(seconds):
    return "{}:{}".format(seconds // 3600, str((seconds % 3600) // 60).zfill(2))
