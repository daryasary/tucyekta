from django.contrib import admin

from partners.models import Partner

from django.utils.translation import ugettext_lazy as _

from reports.models import Activity, UserActivities


class ActivityInline(admin.TabularInline):
    model = Activity
    extra = 0
    fields = ('user_identity', 'get_start', 'get_end', 'pretty_duration')
    readonly_fields = ('user_identity', 'get_start', 'get_end', 'pretty_duration')
    verbose_name = _("Detailed user activities")
    verbose_name_plural = _("Detailed users activities")

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def user_identity(self, obj):
        return obj.user.get_full_name()
    user_identity.short_description = _("user")

    def pretty_duration(self, obj):
        try:
            duration = obj.duration
        except:
            duration = ''
        return duration
    pretty_duration.short_description = _("duration")

    def get_start(self, obj):
        try:
            start = obj.start
        except:
            return ''
        return start
    get_start.short_description = _("start")

    def get_end(self, obj):
        try:
            end = obj.end
        except:
            end = ''
        return end
    get_end.short_description = _("end")


class AggregatedUserActivities(admin.TabularInline):
    model = UserActivities
    extra = 0
    fields = ('user_identity', 'get_total')
    readonly_fields = ('user_identity', 'get_total')
    verbose_name = _("Aggregated user activities")
    verbose_name_plural = _("Aggregated users activities")

    def has_add_permission(self, request):
        return False

    def has_delete_permission(self, request, obj=None):
        return False

    def get_total(self, obj):
        return obj.total()
    get_total.short_description = _("Total")

    def user_identity(self, obj):
        return obj.user.get_full_name()
    user_identity.short_description = _("user")


class PartnerAdmin(admin.ModelAdmin):
    list_display = ['name', 'state', 'duration_sum', 'active']
    list_filter = ['state']
    search_fields = ['name']
    inlines = [AggregatedUserActivities, ActivityInline]

    def duration_sum(self, obj):
        return obj.total_activities
    duration_sum.short_description = _('duration sum')


admin.site.register(Partner, PartnerAdmin)
