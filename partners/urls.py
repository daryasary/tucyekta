from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from partners.views import PartnersListView, PartnerDetailView, \
    PartnerCreateView

urlpatterns = [
    url(r'^(?P<pk>[0-9]+)/$', login_required(PartnerDetailView.as_view()), name='partner_detail'),
    url(r'^$', login_required(PartnersListView.as_view()), name='partners_list'),
    url(r'^create/$', login_required(PartnerCreateView.as_view()), name='partners_create'),
]
