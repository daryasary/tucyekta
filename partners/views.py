from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import ListView, DetailView, CreateView
from django.utils.translation import ugettext_lazy as _

from partners.models import Partner
from reports.forms import ActivityForm
from states.models import State


class PartnersListView(ListView):
    model = Partner
    ordering = '-id'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filtered'] = bool(self.request.GET.get('name', None))
        return context

    def get_queryset(self):
        qs = super().get_queryset()
        name = self.request.GET.get('name', None)
        if name is not None:
            qs = qs.filter(name__contains=name)
        return qs


class PartnerDetailView(DetailView):
    form = ActivityForm
    model = Partner

    def get_initial(self):
        initial = dict(partner=self.kwargs['pk'], allowed_days=2)
        return initial

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = self.form(initial=self.get_initial())
        return context

    def post(self, request, *args, **kwargs):
        form = self.form(request.POST)
        if form.is_valid():
            activity = form.save(commit=False)
            activity.user = request.user
            activity.save()
            return HttpResponseRedirect(reverse('partners_list'))
        self.object = self.get_object()
        context = self.get_context_data(object=self.object, errors=form.non_field_errors())
        return self.render_to_response(context)


class PartnerCreateView(CreateView):
    model = Partner
    fields = ['name', 'state']
    success_url = '/'

    def get_initial(self):
        initial = super().get_initial()
        initial['state'] = State.objects.first()
        return initial

    def form_valid(self, form):
        name = form.cleaned_data.get('name', None)
        if (name is not None) and Partner.objects.filter(name=name).exists():
            form.add_error('__all__', _('Partner with this name already exists'))
            return self.form_invalid(form)
        return super().form_valid(form)
