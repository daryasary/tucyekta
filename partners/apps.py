from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class PartnersConfig(AppConfig):
    name = 'partners'
    verbose_name = _("partner")
    verbose_name_plural = _("partners")
