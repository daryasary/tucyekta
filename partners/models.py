from django.db import models
from django.db.models import Sum
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from partners.utils import prettify
from states.models import State


class Partner(models.Model):
    name = models.CharField(max_length=32, verbose_name=_("name"))
    state = models.ForeignKey(
        State, related_name='partners', verbose_name=_('state')
    )
    active = models.BooleanField(default=True, verbose_name=_('active'))

    created_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("partner")
        verbose_name_plural = _("partners")

    def save(self, **kwargs):
        if self.state is None:
            self.state = State.objects.first()
        return super().save(**kwargs)

    def __str__(self):
        return self.name

    @property
    def set_link(self):
        return reverse('partner_detail', args=[self.id])

    @property
    def total_activities(self):
        activities_time_sum = self.activities.aggregate(Sum('duration_seconds'))
        return prettify(int(activities_time_sum['duration_seconds__sum'] or 0))
