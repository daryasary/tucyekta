from django.core import validators
from django.utils.translation import ugettext as _
from django.contrib.auth import authenticate, get_user_model

from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response
from rest_framework_jwt.settings import api_settings
from rest_framework_jwt.views import JSONWebTokenAPIView, \
    jwt_response_payload_handler
from rest_framework_jwt.serializers import JSONWebTokenSerializer
from rest_framework_jwt.compat import PasswordField

User = get_user_model()
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER
jwt_decode_handler = api_settings.JWT_DECODE_HANDLER


class MyJSONWebTokenSerializer(JSONWebTokenSerializer):
    """
    Serializer class used to validate a username and password.

    'username' is identified by the custom UserModel.USERNAME_FIELD.

    Returns a JSON Web Token that can be used to authenticate later calls.
    """

    def __init__(self, *args, **kwargs):
        """
        Dynamically add the USERNAME_FIELD to self.fields.
        """
        super(MyJSONWebTokenSerializer, self).__init__(*args, **kwargs)

        self.fields[self.username_field] = serializers.CharField()
        self.fields['password'] = PasswordField(write_only=True)
        self.fields['device_uuid'] = serializers.UUIDField(write_only=True)
        # self.fields['notify_token'] = serializers.UUIDField(write_only=True)
        # self.fields['notify_token'] = serializers.CharField(
        #     write_only=True, allow_blank=True,
        #     validators=[
        #         validators.RegexValidator(
        #             r'^([a-z]|[A-z]|[0-9])\w+$',
        #             _('Notify token is not valid'),
        #             'invalid'
        #         )
        #     ]
        # )

    def validate(self, attrs):
        credentials = {
            self.username_field: attrs.get(self.username_field),
            'password': attrs.get('password'),
            'device_uuid': attrs.get('device_uuid'),
            # 'notify_token': attrs.get('notify_token'),
        }
        if attrs.get('notify_token'):
            credentials['notify_token'] = attrs.get('notify_token')

        if all(credentials.values()):
            user = authenticate(**credentials)

            if user:
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg)

                payload = jwt_payload_handler(user)

                return {
                    'token': jwt_encode_handler(payload),
                    'user': user
                }
            else:
                msg = _('Unable to login with provided credentials.')
                raise serializers.ValidationError(msg)
        else:
            msg = _('Must include "{username_field}" and "password".')
            msg = msg.format(username_field=self.username_field)
            raise serializers.ValidationError(msg)


class ObtainJSONWebToken(JSONWebTokenAPIView):
    """
    API View that receives a POST with a user's username and password.

    Returns a JSON Web Token that can be used for authenticated requests.
    """
    serializer_class = MyJSONWebTokenSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            token = serializer.object.get('token')
            response_data = jwt_response_payload_handler(token, user, request)

            return Response(response_data)

        errors = serializer.errors
        if 'device_uuid' in serializer.errors:
            errors['device_uuid'] = 'Invalid device uuid'

        return Response(errors, status=status.HTTP_400_BAD_REQUEST)
