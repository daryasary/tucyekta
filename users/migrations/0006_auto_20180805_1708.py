# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0005_auto_20170515_1503'),
    ]

    operations = [
        migrations.AlterField(
            model_name='device',
            name='notify_token',
            field=models.CharField(validators=[django.core.validators.RegexValidator('([a-z]|[A-z]|[0-9])\\w+', 'Notify token is not valid', 'invalid')], verbose_name='Notification Token', max_length=200, blank=True),
        ),
    ]
