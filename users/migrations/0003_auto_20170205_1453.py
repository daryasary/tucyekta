# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_device'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='verify_codes',
        ),
        migrations.AddField(
            model_name='user',
            name='register_code',
            field=models.IntegerField(null=True, verbose_name='register code', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone_number',
            field=models.BigIntegerField(error_messages={'unique': 'A user with this mobile number already exists.'}, validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')], verbose_name='mobile number', unique=True),
        ),
    ]
