# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0007_userprofile_subscription'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='charkhune_token',
            field=models.TextField(blank=True, verbose_name='Charkhune token'),
        ),
    ]
