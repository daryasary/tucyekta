# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-11-11 11:39
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import re


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0008_userprofile_charkhune_token'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='verify_codes',
            field=models.CharField(blank=True, max_length=30, validators=[django.core.validators.RegexValidator(re.compile('^\\d+(?:\\,\\d+)*\\Z', 32), code='invalid', message='Enter only digits separated by commas.')], verbose_name='verification codes'),
        ),
    ]
