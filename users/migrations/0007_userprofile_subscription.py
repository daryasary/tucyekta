# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0006_auto_20180805_1708'),
    ]

    operations = [
        migrations.AddField(
            model_name='userprofile',
            name='subscription',
            field=models.BooleanField(default=False, verbose_name='Subscription'),
        ),
    ]
