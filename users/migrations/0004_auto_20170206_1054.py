# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0003_auto_20170205_1453'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='register_code',
        ),
        migrations.AddField(
            model_name='device',
            name='register_code',
            field=models.IntegerField(unique=True, null=True, verbose_name='register code', blank=True),
        ),
    ]
