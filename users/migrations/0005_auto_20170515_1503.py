# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.core.validators
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0004_auto_20170206_1054'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('nick_name', models.CharField(max_length=150, verbose_name='nick_name', blank=True)),
                ('avatar', models.ImageField(verbose_name='avatar', upload_to='', blank=True)),
                ('birthday', models.DateField(null=True, verbose_name='birthday', blank=True)),
                ('gender', models.NullBooleanField(verbose_name='gender', help_text='female is False, male is True, null is unset')),
                ('email', models.EmailField(max_length=254, verbose_name='email address', blank=True)),
                ('phone_number', models.BigIntegerField(null=True, verbose_name='mobile number', blank=True, validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')])),
            ],
            options={
                'verbose_name': 'profile',
                'db_table': 'users_profile',
                'verbose_name_plural': 'profiles',
            },
        ),
        migrations.RemoveField(
            model_name='device',
            name='one_signal_id',
        ),
        migrations.RemoveField(
            model_name='device',
            name='register_code',
        ),
        migrations.AddField(
            model_name='device',
            name='device_uuid',
            field=models.UUIDField(null=True, unique=True, verbose_name='Device UUID'),
        ),
        migrations.AddField(
            model_name='device',
            name='notify_token',
            field=models.CharField(max_length=200, verbose_name='Notification Token', blank=True),
        ),
        migrations.AddField(
            model_name='user',
            name='verify_codes',
            field=models.CommaSeparatedIntegerField(max_length=30, verbose_name='verification codes', blank=True),
        ),
        migrations.AlterField(
            model_name='user',
            name='email',
            field=models.EmailField(max_length=254, null=True, blank=True, unique=True, verbose_name='email address'),
        ),
        migrations.AlterField(
            model_name='user',
            name='phone_number',
            field=models.BigIntegerField(error_messages={'unique': 'A user with this mobile number already exists.'}, blank=True, unique=True, null=True, verbose_name='mobile number', validators=[django.core.validators.RegexValidator('^989[0-3,9]\\d{8}$', 'Enter a valid mobile number.', 'invalid')]),
        ),
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(max_length=32, error_messages={'unique': 'A user with that username already exists.'}, validators=[django.core.validators.RegexValidator('^[a-zA-Z][a-zA-Z0-9_\\.]+$', 'Enter a valid username starting with a-z. This value may contain only letters, numbers and underscore characters.', 'invalid')], unique=True, verbose_name='username', help_text='Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only.'),
        ),
        migrations.AlterModelTable(
            name='device',
            table='users_device',
        ),
        migrations.AlterModelTable(
            name='user',
            table='users_user',
        ),
        migrations.AddField(
            model_name='userprofile',
            name='user',
            field=models.OneToOneField(to=settings.AUTH_USER_MODEL),
        ),
    ]
