from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class UsersAppConfig(AppConfig):
    name = 'users'
    verbose_name = _('users')
    verbose_name_plural = _('Users')
