from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _

from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from utils.formats import get_encryption_class
from .models import UserProfile


User = get_user_model()


class ProfileSerializer(serializers.ModelSerializer):
    first_name = serializers.CharField(source='get_first_name', required=False, allow_blank=True)
    last_name = serializers.CharField(source='get_last_name', required=False, allow_blank=True)

    class Meta:
        model = UserProfile
        exclude = ('id', 'user')
        extra_kwargs = {'charkhune_token': {'read_only': True}}

    def validate_email(self, value):
        request = self.context['request']
        user = request.user
        if user.email is not None:
            raise serializers.ValidationError(_('Email has already been set.'))
        if User.objects.filter(email=value).exclude(id=user.id).exists():
            raise serializers.ValidationError(_('User with this Email already exists.'))
        return value

    def validate_phone_number(self, value):
        request = self.context['request']
        user = request.user
        if user.phone_number is not None:
            raise serializers.ValidationError(_('Phone Number has already been set.'))
        if User.objects.filter(phone_number=value).exclude(id=user.id).exists():
            raise serializers.ValidationError(_('User with this Phone Number already exists.'))
        return value

    def update(self, instance, validated_data):
        first_name = validated_data.pop('get_first_name', instance.user.first_name)
        last_name = validated_data.pop('get_last_name', instance.user.last_name)
        instance = super().update(instance, validated_data)
        instance.user.first_name = first_name
        instance.user.last_name = last_name
        instance.user.save(update_fields=['first_name', 'last_name'])
        return instance


class RegisterSerializer(serializers.ModelSerializer):
    phone_number = serializers.CharField(max_length=120)

    class Meta:
        model = User
        fields = ('phone_number',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for i, v in enumerate(self.fields['phone_number'].validators):
            if isinstance(v, UniqueValidator):
                del self.fields['phone_number'].validators[i]

    def create(self, validated_data):
        phone_number = validated_data['phone_number']

        cipher_class = get_encryption_class()
        cipher = cipher_class(settings.ENCRYPTION_KEY)

        try:
            phone_number = cipher.decrypt(phone_number)
        except Exception:
            raise ValidationError(
                {'message': _('phone number is invalid')}, code=400
            )

        if not phone_number:
            raise ValidationError(
                {'message': _('phone number is invalid')}, code=400
            )

        validated_data.update({'phone_number': phone_number})

        try:
            instance = User.objects.get(phone_number=validated_data['phone_number'])
        except User.DoesNotExist:
            instance = User.objects.create_user(phone_number=validated_data['phone_number'], verify_codes=validated_data['verify_code'])
        else:
            instance.set_verify_code(validated_data['verify_code'])
            instance.save(update_fields=['verify_codes'])

        return instance

