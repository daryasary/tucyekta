import random

from django.core.validators import validate_comma_separated_integer_list
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core import validators
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager, send_mail


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, phone_number, email, password, is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not username:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(phone_number=phone_number,
                          username=username, email=email,
                          is_staff=is_staff, is_active=True,
                          is_superuser=is_superuser,
                          date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username=None, phone_number=None, email=None, password=None, **extra_fields):
        if username is None:
            if email:
                username = email.split('@', 1)[0]
            if phone_number:
                username = random.choice('abcdefghijklmnopqrstuvwxyz') + str(phone_number)[-7:]
            while User.objects.filter(username=username).exists():
                username += str(random.randint(10, 99))

        return self._create_user(username, phone_number, email, password, False, False, **extra_fields)

    def create_superuser(self, username, phone_number, email, password, **extra_fields):
        return self._create_user(username, phone_number, email, password, True, True, **extra_fields)

    def get_by_phone_number(self, phone_number):
        return self.get(**{'phone_number': phone_number})


class User(AbstractBaseUser, PermissionsMixin):
    """
    An abstract base class implementing a fully featured User model with
    admin-compliant permissions.

    Username, password and email are required. Other fields are optional.
    """
    username = models.CharField(_('username'), max_length=32, unique=True,
        help_text=_('Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only.'),
        validators=[
            validators.RegexValidator(r'^[a-zA-Z][a-zA-Z0-9_\.]+$',
                                      _('Enter a valid username starting with a-z. '
                                        'This value may contain only letters, numbers '
                                        'and underscore characters.'), 'invalid'),
        ],
        error_messages={
            'unique': _("A user with that username already exists."),
        }
    )
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=30, blank=True)
    email = models.EmailField(_('email address'), unique=True, null=True, blank=True)
    phone_number = models.BigIntegerField(_('mobile number'), unique=True, null=True, blank=True,
        validators=[
            # TODO: use utils.validators
            validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                     ('Enter a valid mobile number.'), 'invalid'),
        ],
        error_messages={
            'unique': _("A user with this mobile number already exists."),
        }
    )
    is_staff = models.BooleanField(_('staff status'), default=False,
        help_text=_('Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(_('active'), default=True,
        help_text=_('Designates whether this user should be treated as active. '
                    'Unselect this instead of deleting accounts.'))
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    verify_codes = models.CharField(verbose_name=_('verification codes'), validators=[validate_comma_separated_integer_list], max_length=30, blank=True)

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'phone_number']

    class Meta:
        db_table = 'users_user'
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def get_full_name(self):
        """
        Returns the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s' % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """
        Returns the short name for the user.
        """
        return self.first_name

    def email_user(self, subject, message, from_email=None, **kwargs):
        """
        Sends an email to this User.
        """
        send_mail(subject, message, from_email, [self.email], **kwargs)

    @property
    def is_loggedin_user(self):
        """
        Returns True if user has actually logged in with valid credentials.
        """
        return self.phone_number is not None or self.email is not None

    def get_verify_codes(self):
        if self.verify_codes:
            return self.verify_codes.split(',')
        else:
            return []

    def set_verify_code(self, verify_code):
        vlist = self.get_verify_codes()
        vlist.append(str(verify_code))
        self.verify_codes = ','.join(vlist[-5:])

    def pop_verify_codes(self, verify_code):
        if (',' + verify_code) in self.verify_codes:
            self.verify_codes = self.verify_codes.replace(',' + verify_code, '')

        elif (verify_code + ',') in self.verify_codes:
            self.verify_codes = self.verify_codes.replace(verify_code + ',', '')

        else:
            self.verify_codes= self.verify_codes.replace(verify_code, '')
        self.save()

    def save(self, *args, **kwargs):
        if self.email is not None and self.email.strip() == '':
            self.email = None
        super().save(*args, **kwargs)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    nick_name = models.CharField(_('nick_name'), max_length=150, blank=True)
    avatar = models.ImageField(_('avatar'), blank=True)
    birthday = models.DateField(_('birthday'), null=True, blank=True)
    gender = models.NullBooleanField(_('gender'), help_text=_('female is False, male is True, null is unset'))
    email = models.EmailField(_('email address'), blank=True)
    phone_number = models.BigIntegerField(_('mobile number'), null=True, blank=True,
        validators=[
            validators.RegexValidator(r'^989[0-3,9]\d{8}$',
                                     _('Enter a valid mobile number.'), 'invalid'),
        ],
    )
    subscription = models.BooleanField(verbose_name=_("Subscription"), default=False)
    charkhune_token = models.TextField(verbose_name=_("Charkhune token"), blank=True)

    class Meta:
        db_table = 'users_profile'
        verbose_name = _('profile')
        verbose_name_plural = _('profiles')

    @property
    def get_first_name(self):
        return self.user.first_name

    @property
    def get_last_name(self):
        return self.user.last_name


class Device(models.Model):
    user = models.ForeignKey(User, related_name='devices')
    device_uuid = models.UUIDField(_('Device UUID'), unique=True, null=True)
    notify_token = models.CharField(
        _('Notification Token'), max_length=200, blank=True,
        validators=[validators.RegexValidator(r'([a-z]|[A-z]|[0-9])\w+',
                    _('Notify token is not valid'), 'invalid')]
    )

    class Meta:
        db_table = 'users_device'
        verbose_name = _('device')
        verbose_name_plural = _('devices')


