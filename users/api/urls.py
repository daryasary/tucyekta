from django.conf.urls import url

from .views import ProfileAPIView, RegisterAPIView, obtain_jwt_token, \
    SubUnSubAPIView, CharkhuneTokenAPIView, CheckSKUAPIView
from rest_framework_jwt.views import refresh_jwt_token


urlpatterns = [
    url(r'^register/', RegisterAPIView.as_view()),
    url(r'^obtain-token/', obtain_jwt_token),
    url(r'^refresh-token/', refresh_jwt_token),

    url(r'^profile/$', ProfileAPIView.as_view(), name='profile-detail'),
    url(r'^profile/subscribe/$', SubUnSubAPIView.as_view({'post': 'subscribe'}), name='subscribe'),
    url(r'^profile/unsubscribe/$', SubUnSubAPIView.as_view({'post': 'unsubscribe'}), name='unsubscribe'),
    url(r'^profile/update_token/$', CharkhuneTokenAPIView.as_view(), name='update_token'),
    url(r'^profile/check_sku/$', CheckSKUAPIView.as_view(), name='check_sku'),
]
