import requests
from random import randint

from django.template.loader import render_to_string
from django.contrib.auth import get_user_model
from django.utils import timezone

from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import detail_route
from rest_framework.exceptions import ValidationError
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from package.models import PurchasePackage
from users.throttles import PhoneNumberScopedRateThrottle, RegisterThrottle
from .drf_jwt import ObtainJSONWebToken
from .serializers import ProfileSerializer, RegisterSerializer
from .models import UserProfile


User = get_user_model()
obtain_jwt_token = ObtainJSONWebToken.as_view()


def send_msg(phone_number, message):
    query_params = {
        'username': "yara6000",
        'password': "yara6000",
        'srcaddress': "98200049103",
        'dstaddress': phone_number,
        'body': message,
        'unicode': 1,
    }

    try:
        r = requests.get("http://ws.adpdigital.com/url/send", params=query_params, timeout=10)
        r.raise_for_status()
    except Exception as e:
        print(e)
        return False

    return True


def send_verification():
    pass


class ProfileAPIView(generics.RetrieveUpdateAPIView):
    serializer_class = ProfileSerializer
    queryset = UserProfile.objects.select_related('user').all()
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    def get_object(self):
        user = self.request.user
        obj, created = UserProfile.objects.select_related('user').get_or_create(
            user=user,
            defaults={
                'nick_name': user.first_name,
                'phone_number': user.phone_number,
                'email': user.email or '',
            }
        )
        return obj

    def perform_update(self, serializer):
        instance = serializer.save()
        if instance.phone_number != instance.user.phone_number:
            send_verification()


class CharkhuneTokenAPIView(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        request.user.userprofile.charkhune_token = request.data['charkhune_token']
        request.user.userprofile.save()
        return Response(status=status.HTTP_201_CREATED)


class SubUnSubAPIView(GenericViewSet):
    serializer_class = ProfileSerializer
    queryset = UserProfile.objects.select_related('user').all()
    authentication_classes = (JSONWebTokenAuthentication, )
    permission_classes = (IsAuthenticated, )

    @detail_route(methods=['POST'])
    def subscribe(self, request):
        user = request.user
        profile = user.userprofile
        profile.subscription = True
        profile.save()
        return Response(status=status.HTTP_201_CREATED)

    @detail_route(methods=['POST'])
    def unsubscribe(self, request):
        user = request.user
        profile = user.userprofile
        profile.subscription = False
        profile.save()
        return Response(status=status.HTTP_201_CREATED)


class RegisterAPIView(generics.CreateAPIView):
    serializer_class = RegisterSerializer
    queryset = User.objects.all()
    throttle_classes = (PhoneNumberScopedRateThrottle, RegisterThrottle)
    throttle_scope = 'register'

    def perform_create(self, serializer):
        # verify_code = "11111"
        verify_code = str(randint(10000, 99999))
        serializer.save(verify_code=verify_code)
        msg = render_to_string(
            'phone_verify.text',
            context={
                'verify_code': verify_code,
            },
        )
        sent_message = send_msg(serializer.data['phone_number'], msg)
        if not sent_message:
            raise ValidationError({'detail': 'Could not sent verification SMS'})


class CheckSKUAPIView(APIView):
    queryset = PurchasePackage.objects.all()

    def post(self, request, *args, **kwargs):
        try:
            user = User.objects.get(phone_number=request.data.get('phone_number'))
        except (User.DoesNotExist, User.MultipleObjectsReturned):
            return Response(
                {"msg": "User does not exists"}, status=status.HTTP_404_NOT_FOUND
            )
        purchase = PurchasePackage.objects.filter(
            package__sku=request.data.get('sku'),
            user=user, expire_at__gte=timezone.now(), valid=True
        ).exists()
        return Response(
            {
                'sku': request.data.get('sku'),
                'phone_number': request.data.get('phone_number'),
                'exists': purchase
            },
            status=status.HTTP_200_OK)
