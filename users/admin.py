from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import Group
# from django.contrib.flatpages.models import FlatPage
# from django.contrib.sites.models import Site
from django.utils.translation import ugettext_lazy as _

# from django.contrib.flatpages.admin import FlatPageAdmin as FlatPageAdminOld
# from django.contrib.flatpages.admin import FlatpageForm as FlatpageFormOld

# from ckeditor.widgets import CKEditorWidget

from .models import User


class MyUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'phone_number', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username', 'phone_number', 'password1', 'password2'),
        }),
    )
    list_display = ('username', 'phone_number', 'email', 'is_staff')
    search_fields = ('username', 'phone_number')


# class FlatpageForm(FlatpageFormOld):
#     widget = CKEditorWidget(config_name='flat')
#     content = forms.CharField(widget=widget)
#
#     class Meta:
#         model = FlatPage  # this is not automatically inherited from FlatpageFormOld
#         fields = '__all__'
#
#
# class FlatPageAdmin(FlatPageAdminOld):
#     form = FlatpageForm


admin.site.unregister(Group)
admin.site.register(User, MyUserAdmin)
