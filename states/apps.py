from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class StatesConfig(AppConfig):
    name = 'states'
    verbose_name = _("States and branch")
    verbose_name_plural = _("States and branches")
