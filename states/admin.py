from django.contrib import admin

from partners.models import Partner
from states.models import State


class PartnersInline(admin.TabularInline):
    model = Partner
    extra = 0
    readonly_fields = ('name', 'active')

    def has_delete_permission(self, request, obj=None):
        return False

    # def has_add_permission(self, request):
    #     return False


class StateAdmin(admin.ModelAdmin):
    list_display = ['name']
    inlines = [PartnersInline]


admin.site.register(State, StateAdmin)
