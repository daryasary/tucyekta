from django.contrib import admin
from django.utils.translation import ugettext_lazy as _

admin.site.site_header = _('Workflow automation')
admin.site.site_title = _('workflow automation')
admin.site.index_title = _('Applications and models')
