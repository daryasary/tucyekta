from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class CustomAdminAppConfig(AppConfig):
    name = 'custom admin'
    verbose_name = _('custom admin')
    verbose_name_plural = _('custom admin')
