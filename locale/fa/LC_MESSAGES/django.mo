��    W      �      �      �  )   �  .   �     �               5     E  9   U  e   �     �          (     4     B     [     w  q   �                 /   #     S     f     �     �  "   �  ]   �     	  "   !	  "   D	     g	     y	     �	     �	     �	  *   �	     �	     �	     �	  $    
  +   %
     Q
     W
  	   h
     r
     {
  	   �
     �
  
   �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                 ,   !  
   N     Y  	   `     j     x  	   }     �     �     �     �     �     �     �     �     �  
   �     �     �     �           	          "  �  6  8   �  B   �  '   A  +   i  $   �     �     �  h   �  D   V  )   �  )   �     �             &   7  9   ^  }   �               :  L   C     �  )   �     �     �  :     j   =     �  C   �  W   �     M     i     �  
   �  
   �  H   �  
   �  "   �  1     6   Q  C   �     �  )   �               .  
   F     Q     Z     n     {     �      �     �     �     �     �     
          )  
   =     H  -   \     �  
   �     �     �     �     �     �     �  -   �     (     7  
   M     X     j     �     �  
   �     �  
   �     �     �     �        A user with that username already exists. A user with this mobile number already exists. Aggregated user activities Aggregated users activities Applications and models Change password Charkhune token Designates whether the user can log into this admin site. Designates whether this user should be treated as active. Unselect this instead of deleting accounts. Detailed user activities Detailed users activities Device UUID Documentation Duration in seconds unit Email has already been set. Enter a valid mobile number. Enter a valid username starting with a-z. This value may contain only letters, numbers and underscore characters. Home Important dates Log out Must include "{username_field}" and "password". Notification Token Notify token is not valid Permissions Personal info Phone Number has already been set. Required. 30 characters or fewer starting with a letter. Letters, digits and underscore only. Start Start and End time is not provided Start time cannot be after and end States and branch States and branches Subscription Today Total Unable to login with provided credentials. User User Activities User account is disabled. User with this Email already exists. User with this Phone Number already exists. Users Users Activities View site Welcome, Workflow automation Yesterday active activities activity avatar birthday custom admin date joined day device devices duration duration sum email address end end time female is False, male is True, null is unset first name gender last name mobile number name nick_name partner partners phone number is invalid profile profiles report reports staff status start start time state states user username users verification codes workflow automation Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-01-24 18:27+0330
PO-Revision-Date: 2019-01-25 13:04+0326
Last-Translator: b'  <hosein@inprobes.com>'
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=1; plural=0;
X-Translated-Using: django-rosetta 0.9.0
 این نام کاربری قبلا ثبت شده است کاربری با این شماره موبایل وجود دارد مجموع کارکرد تکنیکر‌ مجموع کارکرد تکنیکر‌ها برنامه‌ها و مدل‌ها تغییر رمز عبور توکن چارخونه مشخص کنید که کاربر می‌تواند به پنل ادمین لاگین کند یا خیر مشخص کنید که آیا کاربر فعال است یا خیر جزییات کارکرد تکنیکر‌ جزییات کارکرد تکنیکر‌ شناسه دستگاه مستندات مدت زمان(به ثانیه) ایمیل قبلا ست شده است یک شماره موبایل معتبر وارد کنید یک نام کاربری معتبر وارد کنید که از حروف کوچک، اعداد و ـ تشکیل شده است خانه تاریخ‌های مهم خروج باید "{username_field}"  و "رمز عبور" را وارد نمایید توکن نوتیفیکیشن توکن ارسالی معتبر نیست اجازه‌ها اطلاعات شخصی شماره موبایل قبلا تنظیم شده است. حداکثر ۳۰ کارکاتری که شامل حروف بزرگ، کوچوک اعداد و ـ باشد. آغاز ساعت شروع یا پایان به درستی وارد نشده ساعت وارد شده برای ابتدا و انتها بازه اشتباه است استان‌ها و شعب استان‌ها و شعب اشتراک امروز مجموع قادر به ورود با اطلاعات وارد شده نیستید. کاربر فعالیت‌های تکنیکر حساب کاربری مسدود شده‌است. کاربری با این ایمیل موجود است. کاربری با این شماره موبایل موجود است. کاربران فعالیت‌های تکنیکر‌ها نمایش سایت خوش آمدید،  تحلیل کارکرد دیروز فعال فعالیت‌ها فعالیت تصویر پروفایل تاریخ تولد ادمین پنل اختصاصی تاریخ پیوستن روز دستگاه دستگاه‌ها مدت مجموع کارکرد آدرس ایمیل پایان زمان پایان مونث منفی و مذکر مثبت است نام جنسیت نام خانوادگی شماره موبایل نام نام مشتری‌ مشتری‌ها شماره موبایل نامعتبر است پروفایل پروفایل‌ها گزارش گزارش‌ها وضعیت کارمندی آغاز زمان آغاز استان استان‌ها کاربر نام کاربری کاربران کد اعتبارسنجی تحلیل کارکرد 